import os.path

LIVES=0
DIES=1
SPAWNS=2

ERROR_CODE_NO_USER=1


class AqUser(object):
	login='test'

	aquariums=[]

	def update(self):
		for aq in self.aquariums:
			aq.update()

	def get_status(self):
		return (self.aquariums)

	def add_aquarium(self,aquarium):
		aquariums.append(aquarium)

	def save(self,stream):
		import json
		json.dump([login],stream)
		for aq in aquariums:
			aq.save(stream)

	def load(self,stream):
		import json
		jsonUser= json.load([],stream)
		self.login=jsonUser[login]


	def set_init(self,username):
		self.login=username

	def test_user(self):
		test_aq=AqAquarium()
		test_aq.fishes.append(AqFish(AqFishProto()))
		test_aq.fishes.append(AqFish(AqFishProto()))
		test_aq.fishes.append(AqFish(AqFishProto()))
		test_aq.fishes.append(AqFish(AqFishProto()))
		self.aquariums.append(test_aq)

	def __init__(self):
		self.login=None

	@staticmethod
	def try_load(username,aq_user):
		if (os.path.isfile('./users/'+username+'.user')):
			"""
			loading routine
			"""
			stream=open('./users/'+username+'.user')
			aq_user.load(stream)
			return True
		return False




class AqAquarium(object):
	volume=60

	fishes=[]

	def update(self):
		for fish in self.fishes:
			fish_res=fish.update()
			if (fish_res==DIES):
				self.fishes.remove(fish)
			elif (fish_res==SPAWNS):
				self.fishes.append(fish.spawn())
			elif (fish_res==LIVES):
				pass

	def __str__(self):
		return str(self.fishes)

	def save(self,stream):
		import json
		json.dump([volume],stream)
		for fi in fishes:
			fi.save(stream)

class AqFishProto(object):
	minLifeExpectancy=24
	maxLifeExpectancy=36
	waterVolumeNeeded=2.0
	spawningPeriod=6
	adultingTime=7
	childDeathRate=.3
	name="Guppy?"
	def __str__(self):
		return self.name+" " +str(self.minLifeExpectancy) + " "

class AqFish(object):
	aqFishProto=None
	lifeExpectancy=24
	spawningPeriodLeft=0
	age=0

	def update(self):
		self.age+=1
		if (self.age>=self.lifeExpectancy):
			return DIES

		if (self.age<=self.aqFishProto.adultingTime):
			import random
			if random.random()<self.aqFishProto.childDeathRate/self.aqFishProto.adultingTime*2:
				return DIES


		if (self.age>self.aqFishProto.adultingTime):
			if (self.spawningPeriodLeft<=0):
				self.spawningPeriodLeft=self.aqFishProto.spawningPeriod
				return SPAWNS
			else:
				self.spawningPeriodLeft-=1
		return LIVES


	def __str__(self):
		return str(self.aqFishProto) + " " + str(self.spawningPeriodLeft) + " " + str(self.age)

	def __init__(self):
	    pass

	#def __init__(self,proto):
	#	self.aqFishProto=proto

	#def save(self,stream):
	#	import json
	#	json.dump([lifeExpectancy,waterVolumeNeeded,spawningPeriod,spawningPeriodLeft,age,adultingTime,childDeathRate],stream)


class AqModel(object):
	def __init__(self):
		pass

	def get_user(self,username):
		aq_user=AqUser()
		if (not AqUser.try_load(username,aq_user)):
			#return ERROR_CODE_NO_USER
			self.register_user(username,aq_user)
		return aq_user

	def register_user(self,username,aq_user):
		aq_user.set_init(username) #__init__(username)
		aq_user.test_user()
