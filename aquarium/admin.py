from django.contrib import admin
#import models

from .models import AqFishProto,AqFish,AqAquarium

admin.site.register(AqFishProto)
admin.site.register(AqFish)
admin.site.register(AqAquarium)


"""
class AqFishProtoAdmin(admin.ModelAdmin):
	pass

class AqFishAdmin(admin.ModelAdmin):
	pass

class AqAquariumAdmin(admin.ModelAdmin):
	pass



class YourModelAdmin(admin.ModelAdmin):
	pass

admin.site.register(models.AqFishProto, AqFishProtoAdmin)
admin.site.register(models.AqFish, AqFishAdmin)
admin.site.register(models.AqAquarium, AqAquariumAdmin)
"""
