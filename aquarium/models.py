from __future__ import unicode_literals

from django.db import models
from django.contrib.auth import models as auth_models
# Create your models here.

LIVES=0
DIES=1
SPAWNS=2

class AqFishProto(models.Model):
	minLifeExpectancy=models.IntegerField()
	maxLifeExpectancy=models.IntegerField()
	waterVolumeNeeded=models.DecimalField(decimal_places=3,max_digits=20)
	spawningPeriod=models.IntegerField()
	adultingTime=models.IntegerField()
	childDeathStopAge=models.IntegerField()
	childDeathRate=models.FloatField()
	name=models.CharField(max_length=120)
	minSpawnCount=models.IntegerField()
	maxSpawnCount=models.IntegerField()
	def __unicode__(self):
		return self.name.encode('utf-8')

class AqAquarium(models.Model):
	volume=models.DecimalField(decimal_places=3,max_digits=20)
	name=models.CharField(max_length=64,default="Aquarium")
	age=models.IntegerField(default=0)
	owner=models.ForeignKey( auth_models.User(),default=None)
	
	def getOccupiedVolume(self):
		occ=0.0
		for fi in self.findFishes():
			occ+=float(fi.proto.waterVolumeNeeded)
		return occ
	
	def getChildrenAmount(self):
		chil=[]
		for fi in self.findFishes():
			if fi.age<fi.proto.adultingTime:
				chil.append(fi)
		return len(chil)
	
	def getFemaleAmount(self):
		fem=[]
		for fi in self.findFishes():
			if fi.isFemale:
				fem.append(fi)
		return len(fem)
	
	def findFishes(self):
		return AqFish.objects.filter(aquarium=self)

	def addFishFromProto(self,proto):
		fi=AqFish()
		fi.proto=proto
		fi.aquarium=self
		fi.age=0
		fi.lastSpawnedAge=0
		import random
		fi.lifeExpectancy=proto.minLifeExpectancy+ random.random()*(proto.maxLifeExpectancy-proto.minLifeExpectancy)
		isFem=random.random()>0.5
		fi.isFemale=isFem
		fi.save()

	def update(self):
		freeVol=self.volume
		self.age+=1
		for fi in self.findFishes():
			freeVol-=fi.proto.waterVolumeNeeded
			if freeVol<0:
				fi.delete()
				continue
			fish_res=fi.update()
			if fish_res==DIES:
				fi.delete()
				continue
			elif (fish_res==SPAWNS):
				spawned=fi.spawn()
				if (spawned!=None):
					for fi in spawned:
						freeVol-=fi.proto.waterVolumeNeeded
						if (freeVol>0):
							fi.save()
				continue
			elif (fish_res==LIVES):
				continue
		self.save()

	def __unicode__(self):
		return self.name.encode('utf-8')

class AqFish(models.Model):
	def spawn(self):
		if (not self.isFemale):
			return
		import random

		spawnC=int(random.random()*(self.proto.maxSpawnCount- self.proto.minSpawnCount)+self.proto.minSpawnCount)
		sfish=[]
		for i in range(spawnC):
			spawnedFish= AqFish()
			spawnedFish.proto=self.proto
			spawnedFish.aquarium=self.aquarium
			spawnedFish.age=0
			spawnedFish.lastSpawnedAge=0
			spawnedFish.lifeExpectancy=self.proto.minLifeExpectancy+ random.random()*(self.proto.maxLifeExpectancy-self.proto.minLifeExpectancy)
			isFem=random.random()>0.4
			spawnedFish.isFemale=isFem
			sfish.append(spawnedFish)
		return sfish

	def update(self):
		self.age+=1
		if (self.age>=self.lifeExpectancy):
			return DIES

		if (self.age<=self.proto.childDeathStopAge):
			import random,math
			deathProb=(1-math.sqrt(1-4*self.proto.childDeathRate/self.proto.childDeathStopAge))/2
			if random.random()<deathProb:
				return DIES


		if (self.age>self.proto.adultingTime):
			if (self.proto.spawningPeriod+ self.lastSpawnedAge-self.age<=0):
				self.lastSpawnedAge=self.age
				self.save()
				return SPAWNS
		self.save()
		return LIVES

	aquarium=models.ForeignKey(AqAquarium)
	proto=models.ForeignKey(AqFishProto)

	isFemale=models.BooleanField()

	age=models.IntegerField()
	lastSpawnedAge=models.IntegerField()
	lifeExpectancy=models.IntegerField()
