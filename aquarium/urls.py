from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from . import views

urlpatterns = [#'',
	# Examples:
	# url(r'^$', 'aquarium.views.home', name='home'),
	# url(r'^blog/', include('blog.urls')),

	url(r'^login',views.login_subr),

	url(r'^admin/', include(admin.site.urls)),
	url(r'^$', views.index, name="index"),
	url(r'^status/{0,1}', views.aqs_status),

	url(r'^process',views.aq_process_action),
	url(r'^aqedit/([0-9]+)',views.aq_edit),
	url(r'^aqdelete/([0-9]+)',views.aq_delete),
	url(r'^aquarium/([0-9]+)',views.aq_status),
	url(r'^aqadd',views.aq_add),
	url(r'^fishes',views.fish_types),
	
	url(r'.*', views.default)
]
