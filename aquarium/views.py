from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login

# Create your views here.

from django import forms
from .models import AqFishProto,AqAquarium
class AddFishForm(forms.Form):
	fishProto=forms.ModelChoiceField(queryset=AqFishProto.objects.all(),label="Fish to add")

from django.http import HttpResponse,HttpResponseRedirect

def index(request):
	return (render(request,"index.html"))
	
def default(request):
	return HttpResponse("Hi. This is a default page for non-recognized page. You requested: \"" + request.path + "\".")

@login_required(login_url="/login")
def aqs_status(request):
	from .models import AqAquarium
	return render(request,"aqs_status.html", {"aqs":AqAquarium.objects.filter(owner=request.user)})
	
def aq_process_action(request):
	if "register" in request.POST:
		from django.contrib.auth.models import User
		from django.contrib.auth import authenticate, login
		us_name=request.POST["login"]
		us_pass=request.POST["password"]
		same_name_us=User.objects.filter(username=us_name)
		if len(same_name_us)>0:
			return HttpResponseRedirect("/login")
		newUser=User.objects.create_user(username=us_name,password=us_pass)
		if newUser!=None and newUser.is_active:
			newUser=authenticate(username=us_name,password=us_pass)
			login(request,newUser)			
			return HttpResponseRedirect("/status")
		return HttpResponseRedirect("/login")
	elif not request.user.is_authenticated():
		return HttpResponseRedirect("/login")
	elif "update" in request.POST:
		return aq_update(request)
	elif "logout" in request.POST:
		from django.contrib.auth import logout
		logout(request)
		return HttpResponseRedirect("/")
	elif "delete" in request.POST:
		return HttpResponseRedirect("/aqdelete/"+request.POST['aq_id'])
	elif "suredelete" in request.POST:
		if (request.POST["suredelete"]=="yes"):
			from .models import AqAquarium
			aqs=AqAquarium.objects.filter(id=request.POST["aq_id"],owner=request.user)
			if (len(aqs)>0):
				aq=aqs[0]
				aq.delete()
		return HttpResponseRedirect("/status")
	elif "edit" in request.POST:
		return HttpResponseRedirect("/aqedit/"+request.POST['aq_id'])
	elif "clear" in request.POST:
		from .models import AqAquarium
		aqs=AqAquarium.objects.filter(id=request.POST["aq_id"],owner=request.user)
		if (len(aqs)>0):
			aq=aqs[0]
			for fi in aq.findFishes():
				fi.delete()
			aq.age=0
			aq.save()
		return HttpResponseRedirect("/status")
	elif "savech" in request.POST:
		from .models import AqAquarium
		aqs=AqAquarium.objects.filter(id=request.POST["aq_id"],owner=request.user)
		aq=None
		if (len(aqs)>0):
			aq=aqs[0]
			aq.volume=request.POST["volume"]
			aq.name=request.POST["name"]
			aq.save()
		return HttpResponseRedirect("/status")
	elif "addfish" in request.POST:
		from .models import AqAquarium,AqFishProto
		aqs=AqAquarium.objects.filter(id=request.POST["aq_id"],owner=request.user)
		if (len(aqs)>0):
			aq=aqs[0]
			protos=AqFishProto.objects.filter(id=request.POST["fishProto"])
			proto=None
			if len(protos)>0:
				proto = protos[0]
			if proto!=None:
				aq.addFishFromProto(proto)
		return HttpResponseRedirect("/aquarium/"+request.POST["aq_id"])
	elif "move" in request.POST:
		from .models import AqAquarium,AqFish
		fromAquariums=AqAquarium.objects.filter(id=request.POST["aq_id"],owner=request.user)
		if len(fromAquariums)>0:
			fis=AqFish.objects.filter(id=request.POST["fish_id"],aquarium=fromAquariums[0])
			targetAquariums=AqAquarium.objects.filter(id=request.POST["targetAquarium"],owner=request.user)
			if len(targetAquariums)>0:
				fi.aquarium=targetAquariums[0]
				fi.save()
		return HttpResponseRedirect("/status")

@login_required(login_url="/login")
def fish_types(request):
	from .models import AqFishProto,AqAquarium
	protos=AqFishProto.objects.all()
	aqs=AqAquarium.objects.filter(owner=request.user)
	protoInfo=[]
	for pro in protos:
		protoCount=0
		protoSmallCount=0
		protoFemCount=0
		for aq in aqs:
			fishes=aq.findFishes()
			for fi in fishes:
				if fi.proto==pro:
					protoCount+=1
					if fi.age<pro.adultingTime:
						protoSmallCount+=1
					if fi.isFemale:
						protoFemCount+=1
						
		protoInfo.append([pro, protoCount, protoSmallCount, protoFemCount])
	return render(request,"fishes.html",
		{
			"proto_infos":protoInfo
		})	

@login_required(login_url="/login")
def aq_status(request,aq_id):
	from .models import AqAquarium
	from .models import AqAquarium
	aq=None
	aqs=AqAquarium.objects.filter(id=aq_id,owner=request.user)
	
	moveForm=forms.Form()
	moveForm.targetAquarium=forms.ModelChoiceField(AqAquarium.objects.filter(owner=request.user),label="Move fish to")
	moveForm.fields['targetAquarium']=moveForm.targetAquarium
	stat={}
	if len(aqs)>0:
		aq=aqs[0]
		
		for fi in aq.findFishes():
			if not stat.has_key(fi.proto.id):
				stat[fi.proto.id]=[fi.proto.name,0,0,0]
			if fi.isFemale:
				stat[fi.proto.id][1]+=1
			if fi.age<fi.proto.adultingTime:
				stat[fi.proto.id][2]+=1
			stat[fi.proto.id][3]+=1
	
	return render(request,"aq_status.html",
		{
			"aq":aq,
			"add_fish_form":AddFishForm(),
			"move_fish_form":moveForm,
			"stat":stat
		})

@login_required(login_url="/login")
def aq_add(request):
	from .models import AqAquarium
	aq=AqAquarium()
	aq.name=request.POST["name"]
	aq.volume=request.POST["volume"]
	aq.owner=request.user
	aq.save()
	return HttpResponseRedirect("/status")

@login_required(login_url="/login")
def aq_edit(request,aq_id):
	from .models import AqAquarium
	aqs=AqAquarium.objects.filter(id=aq_id,owner=request.user)
	aq=None
	if (len(aqs)>0):
		aq=aqs[0]
	return render(request,"aq_edit.html",{
		"aq":aq})

@login_required(login_url="/login")
def aq_delete(request,aq_id):
	from .models import AqAquarium
	aq=None
	aqs=AqAquarium.objects.filter(id=aq_id,owner=request.user)
	if (len(aqs)>0):
		aq=aqs[0]
	return render(request,"aq_delete.html",{"aq":aq})
	return HttpResponse("this is an aquarium delete page")

@login_required(login_url="/login")
def aq_update(request):
	if (request.method=="POST"):
		from .models import AqAquarium
		aqs=AqAquarium.objects.filter(id=request.POST['aq_id'],owner=request.user)
		if len(aqs)>0:
			aqAq=aqs[0]
			aqAq.update()
	return HttpResponseRedirect(request.POST["from"])

def login_subr(request):
	if (request.method=="GET"):
		return render(request,"login.html")
	
	username=request.POST["login"]
	password=request.POST["password"]
	from django.contrib.auth import authenticate, login
	user = authenticate(username=username, password=password)
	if user is not None:
		# the password verified for the user
		if user.is_active:
			login(request,user)
			return HttpResponseRedirect("/status")
		else:
			return HttpResponseRedirect("/status")
	else:
		return HttpResponseRedirect("/status")
			
	return HttpResponse("Unknown situation")
